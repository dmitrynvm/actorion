import os

SMPL_DATA_PATH = 'resources'
SMPL_KINTREE_PATH = os.path.join(SMPL_DATA_PATH, 'kintree_table.pkl')
SMPL_MODEL_PATH = os.path.join(SMPL_DATA_PATH, 'SMPL_NEUTRAL.pkl')
JOINT_REGRESSOR_TRAIN_EXTRA = os.path.join(SMPL_DATA_PATH, 'J_regressor_extra.npy')

JOINTSTYPES = ['smpl', 'vibe', 'vertices']
LOSSES = ['rc', 'kl', 'rcxyz']
MODELTYPES = ['cvae']
ARCHINAMES = ['transformer']

POSE_REPS = ['xyz', 'rotvec', 'rotmat', 'rotquat', 'rot6d']

SMPL_MODEL_DIR = 'resources'