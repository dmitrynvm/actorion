import os
import torch
from torch.utils.data import DataLoader
from actorion.train.trainer import train
from actorion.utils.math import collate
from actorion.parser.training import parser
from actorion.datasets.get_dataset import get_datasets
from actorion.models.get_model import get_model


def do_epochs(model, datasets, parameters, optimizer):
    dataset = datasets['train']
    train_iterator = DataLoader(dataset, batch_size=parameters['batch_size'], shuffle=True, num_workers=8, collate_fn=collate)
    for epoch in range(1, parameters['num_epochs']+1):
        dict_loss = train(model, optimizer, train_iterator, model.device)
        for key in dict_loss.keys():
            dict_loss[key] /= len(train_iterator)
        if ((epoch % parameters['snapshot']) == 0) or (epoch == parameters['num_epochs']):
            checkpoint_path = os.path.join(parameters['folder'], 'checkpoint_{:04d}.pth.tar'.format(epoch))
            print('Saving checkpoint {}'.format(checkpoint_path))
            torch.save(model.state_dict(), checkpoint_path)


if __name__ == '__main__':
    parameters = parser()
    datasets = get_datasets(parameters)
    model = get_model(parameters)
    optimizer = torch.optim.AdamW(model.parameters(), lr=parameters['lr'])
    print('Total params: %.2fM' % (sum(p.numel() for p in model.parameters()) / 1000000.0))
    do_epochs(model, datasets, parameters, optimizer)
