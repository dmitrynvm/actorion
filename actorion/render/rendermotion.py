import imageio
import os
import torch
import argparse
import numpy as np
from tqdm import tqdm
import actorion.utils.rotation as geometry
from actorion.render.renderer import get_renderer


def get_rotation(theta=np.pi/3):
    axis = torch.tensor([0, 1, 0], dtype=torch.float)
    axisangle = theta*axis
    matrix = geometry.axis_angle_to_matrix(axisangle)
    return matrix.numpy()


def render_video(meshes, key, action, renderer, savepath, background, cam=(0.75, 0.75, 0, 0.10), color=[0.11, 0.53, 0.8]):
    writer = imageio.get_writer(savepath, fps=30)
    # center the first frame
    meshes = meshes - meshes[0].mean(axis=0)
    imgs = []
    for mesh in tqdm(meshes, desc=f'Visualize {key}, action {action}'):
        img = renderer.render(background, mesh, cam, color=color)
        imgs.append(img)
        # show(img)

    imgs = np.array(imgs)
    masks = ~(imgs/255. > 0.96).all(-1)

    coords = np.argwhere(masks.sum(axis=0))
    y1, x1 = coords.min(axis=0)
    y2, x2 = coords.max(axis=0)

    for cimg in imgs[:, y1:y2, x1:x2]:
        writer.append_data(cimg)
    writer.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    opt = parser.parse_args()
    filename = opt.filename
    savefolder = os.path.splitext(filename)[0]
    os.makedirs(savefolder, exist_ok=True)
    output = np.load(filename, allow_pickle=True)
    visualization, generation, reconstruction = output
    output = {
        'visualization': visualization,
        'generation': generation,
        'reconstruction': reconstruction
    }
    width = 1024
    height = 1024
    background = np.zeros((height, width, 3))
    renderer = get_renderer(width, height)
    for key in output:
        vidmeshes = output[key]
        for action in range(len(vidmeshes)):
            meshes = vidmeshes[action].transpose(2, 0, 1)
            path = os.path.join(savefolder, 'action{}_{}.mp4'.format(action, key))
            render_video(meshes, key, action, renderer, path, background)


if __name__ == '__main__':
    main()
