import importlib
from actorion.models.transformer import Encoder, Decoder
from actorion.models.cvae import CVAE


def get_model(parameters):
    encoder = Encoder(**parameters)
    decoder = Decoder(**parameters)
    parameters['outputxyz'] = 'rcxyz' in parameters['lambdas']
    return CVAE(encoder, decoder, **parameters).to(parameters['device'])
