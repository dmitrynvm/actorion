import torch
import actorion.utils.rotation as geometry
from actorion.models.smpl import SMPL, JOINTSTYPE_ROOT
from actorion.config import JOINTSTYPES


class Rotation2xyz:
    def __init__(self, device):
        self.device = device
        self.smpl_model = SMPL().eval().to(device)

    def __call__(
        self, 
        x, 
        mask, 
        pose_rep, 
        translation, 
        glob,
        jointstype, 
        vertstrans, 
        betas=None, 
        beta=0,
        glob_rot=None, 
        **kwargs
    ):
        if mask is None:
            mask = torch.ones((x.shape[0], x.shape[-1]), dtype=bool, device=x.device)

        x_translations = x[:, -1, :3]
        x_rotations = x[:, :-1]

        x_rotations = x_rotations.permute(0, 3, 1, 2)
        nsamples, time, njoints, feats = x_rotations.shape

        rotations = geometry.rotation_6d_to_matrix(x_rotations[mask])

        global_orient = rotations[:, 0]
        rotations = rotations[:, 1:]

        out = self.smpl_model(
            global_orient=global_orient, 
            body_pose=rotations, 
            betas=betas
        )

        joints = out[jointstype]
        x_xyz = torch.empty(nsamples, time, joints.shape[1], 3, device=x.device, dtype=x.dtype)
        x_xyz[~mask] = 0
        x_xyz[mask] = joints
        x_xyz = x_xyz.permute(0, 2, 3, 1).contiguous()

        # the first translation root at the origin on the prediction
        if jointstype != 'vertices':
            rootindex = JOINTSTYPE_ROOT[jointstype]
            x_xyz = x_xyz - x_xyz[:, [rootindex], :, :]

        if translation and vertstrans:
            # the first translation root at the origin
            x_translations = x_translations - x_translations[:, :, [0]]
            # add the translation to all the joints
            x_xyz = x_xyz + x_translations[:, None, :, :]
        return x_xyz
