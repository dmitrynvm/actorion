import os
import torch
import numpy as np
from actorion.datasets.get_dataset import get_datasets
from actorion.models.get_model import get_model
from actorion.parser.generate import parser
from actorion.utils.math import fixseed 


def generate_actions(
    beta, 
    model, 
    dataset, 
    epoch, 
    params, 
    folder, 
    num_frames=60,
    writer=None
):
    model.param2xyz['jointstype'] = 'vertices'
    classes = torch.arange(dataset.num_classes)
    gendurations = torch.tensor([num_frames for cl in classes], dtype=int)

    # generate the repr (joints3D/pose etc)
    model.eval()
    with torch.no_grad():
        # Generate the new data
        generation = model.generate(
            classes, 
            gendurations, 
            nspa=3,
            noise_same_action='random',
            noise_diff_action='random'
        )
        generation['output_xyz'] = model.rot2xyz(
            generation['output'],
            generation['mask'], 
            vertstrans=False,
            beta=beta
        )
        output = generation['output_xyz'].reshape(3, dataset.num_classes, *generation['output_xyz'].shape[1:]).cpu().numpy()
    print(generation['output_xyz'].shape, output.shape)
    return output


if __name__ == '__main__':
    parameters, folder, checkpointname, epoch = parser()
    datasets = get_datasets(parameters)
    model = get_model(parameters)
    dataset = datasets['train']
    checkpointpath = os.path.join(folder, checkpointname)
    state_dict = torch.load(checkpointpath, map_location=parameters['device'])
    model.load_state_dict(state_dict)

    fixseed(1)
    beta = 0
    output = generate_actions(
        beta, 
        model, 
        dataset, 
        epoch, 
        parameters,
        folder, 
    )
    np.save(os.path.join(folder, 'generation.npy'), output)
