export CUDA_VISIBLE_DEVICES="2"
python -m actorion.train.train_cvae --modelname cvae_transformer_rc_rcxyz_kl --pose_rep rot6d --lambda_kl 1e-5 --jointstype vertices --batch_size 20 --num_frames 60 --num_layers 8 --lr 0.0001 --glob --translation --no-vertstrans --dataset humanact12 --num_epochs 2000 --snapshot 1000 --folder output
#python -m actorion.generate.generator output/checkpoint_0200.pth.tar
#python -m actorion.render.rendermotion output/generation.npy
